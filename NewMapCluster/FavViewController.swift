//
//  FavViewController.swift
//  NewMapCluster
//
//  Created by mac on 9/20/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit

class FavViewController: UIViewController , UITableViewDelegate , UITableViewDataSource{
    
    
    @IBOutlet weak var tableView: UITableView!
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.countries.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! FavTableViewCell
        cell.name.text = self.countries[indexPath.row]
        return cell
    }
    
    var countries : [String] = [String]()
    override func viewDidLoad() {
        super.viewDidLoad()
        print(countries)
    }
    override func viewWillAppear(_ animated: Bool) {
        
        if self.countries.count == 0 {
            tableView.isHidden = true
            
        }
        else {
            tableView.isHidden = false
            
        tableView.delegate = self
        tableView.dataSource = self
        self.tableView.reloadData()
         print(countries)
        }
    }
  
    @IBAction func clear(_ sender: Any) {
        self.countries.removeAll()
        self.tableView.reloadData()
        self.tableView.isHidden = true
    }
    

 

}

//
//  ViewController.swift
//  NewMapCluster
//
//  Created by mac on 9/20/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import UIKit
import MapKit
import Cluster
import SwiftyJSON


class ViewController: UIViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    let manager = ClusterManager()
    var favoriteCountries : [String] = [String]()
    let center = CLLocationCoordinate2D(latitude: 30.047245, longitude: 31.224825 ) // region center
    let delta = 8.1 // region span
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // When zoom level is quite close to the pins, disable clustering in order to show individual pins and allow the user to interact with them via callouts.
        mapView.region = .init(center: center, span: .init(latitudeDelta: delta, longitudeDelta: delta))
        manager.cellSize = nil
        manager.maxZoomLevel = 10
      //  manager.minCountForClustering = 3
        manager.clusterPosition = .nearCenter
        addAnnotations()
    }
    
    
    @IBAction func clear(_ sender: Any) {
        self.favoriteCountries.removeAll()
    }
    
    @IBAction func addAnnotations(_ sender: UIButton? = nil) {
        // Add annotations to the manager.
        
        if let path = Bundle.main.path(forResource: "country-codes-lat-long-alpha3", ofType: "json") {
            do {
                
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                
                
                let jsonObj =  try JSON(data: data)
                print("jsonData:\(jsonObj)")
                let jsonString = String(data: data, encoding: String.Encoding.utf8)
                let response =  RootClass(json: jsonString)
                for c in response.ref_country_codes {
                let annotations: [Annotation] = (0..<response.ref_country_codes.count ).map { i in
                    let annotation = Annotation()
                    
                    let  long = Double(c.longitude)
                    let  lat = Double(c.latitude)
                        annotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: long)
                        let color = UIColor(red: 0/255, green: 0/255, blue: 255/255, alpha: 1)
                        annotation.style = .color(color, radius: 25)
                    
                        annotation.title = c.country
                        // or
                        // annotation.style = .image(UIImage(named: "pin")?.filled(with: color)) // custom image
                     return annotation
                    }
                    manager.add(annotations)
                    manager.reload(mapView: mapView)
                }
                
                
            } catch let error {
                print("parse error: \(error.localizedDescription)")
            }
        } else {
            print("Invalid filename/path.")
        }
        
    }
    
    @IBAction func removeAnnotations(_ sender: UIButton? = nil) {
        manager.removeAll()
        manager.reload(mapView: mapView)
    }
    
}

extension ViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if let annotation = annotation as? ClusterAnnotation {
            guard let style = annotation.style else { return nil }
            let identifier = "Cluster"
            var view = mapView.dequeueReusableAnnotationView(withIdentifier: identifier)
            if let view = view as? BorderedClusterAnnotationView {
                view.annotation = annotation
                
                annotation.title = annotation.annotations[0].title!
                view.style = style
                view.configure()
            } else {
            }
            return view
        } else {
            guard let annotation = annotation as? Annotation, let style = annotation.style else { return nil }
            let identifier = "Pin"
            var view = mapView.dequeueReusableAnnotationView(withIdentifier: identifier) as? MKPinAnnotationView
            if let view = view {
                view.annotation = annotation
            } else {
                view = MKPinAnnotationView(annotation: annotation, reuseIdentifier: identifier)
            }
            if case let .color(color, _) = style {
                view?.pinTintColor = color
            } else {
                view?.pinTintColor = .green
            }
            return view
        }
    }
    
    func mapView(_ mapView: MKMapView, regionDidChangeAnimated animated: Bool) {
        manager.reload(mapView: mapView) { finished in
            print(finished)
        }
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation else { return }
        
        
        if let cluster = annotation as? ClusterAnnotation {
            cluster.title = cluster.annotations[0].title!
            self.favoriteCountries.append(cluster.title!)
            var secondTab = self.tabBarController?.viewControllers![1] as! UINavigationController
            let vc = secondTab.topViewController as! FavViewController
            
            vc.countries = self.favoriteCountries
        }
    }
    

    
 
    
}


class BorderedClusterAnnotationView: ClusterAnnotationView {
    let borderColor: UIColor

    init(annotation: MKAnnotation?, reuseIdentifier: String?, style: ClusterAnnotationStyle, borderColor: UIColor) {
        self.borderColor = borderColor
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier, style: style)
    }

    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")

    }

    override func configure() {
        super.configure()

        switch style {
        case .image:
            layer.borderWidth = 0
        case .color:
            layer.borderColor = borderColor.cgColor
            layer.borderWidth = 2
        }
    }
}



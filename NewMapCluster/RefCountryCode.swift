//
//  File.swift
//  MarkerClustering
//
//  Created by mac on 9/19/18.
//  Copyright © 2018 EasyDevs. All rights reserved.
//

import Foundation
import EVReflection

class RefCountryCode : EVObject{
    
    var alpha2 : String = ""
    var alpha3 : String = ""
    var country : String = ""
    var latitude : Int = 0
    var longitude : Int = 0
    var numeric : Int = 0
    
    
}
